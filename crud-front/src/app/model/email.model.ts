import { Cliente } from './cliente.model';
export interface IEmail {

  id?: number;
  email?: string;
  cliente?: Cliente;


}
export class Email implements IEmail {
  constructor(
    public id?: number,
    public email?: string,
    public cliente?:Cliente
  ){}

}
