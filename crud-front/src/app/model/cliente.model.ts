import { Email } from './email.model';
import { Endereco } from './endereco.model';
import { Telefone } from './telefone.model';
export interface ICliente {

  id?: number;

  nome?: string;

  cpf?: string;

  endereco?: Endereco;

  telefone?: Telefone[];

  email?: Email[];

}
export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public nome?: string,
    public cpf?: string,
    public endereco?: Endereco,
    public telefone?: Telefone[],
    public email?: Email[]
  ){}

}
