import { Cliente } from "./cliente.model";

export interface ITelefone {

  id?: number;
  numero?: string;
  codigoDeArea?: string;
  cliente?: Cliente;


}
export class Telefone implements ITelefone {
  constructor(
    public id?: number,
    public numero?: string,
    public codigoDeArea?: string,
    public cliente?: Cliente,
  ){}


}
