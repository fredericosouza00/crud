import { Cliente } from './cliente.model';

export class Endereco{

    id: number;

    cep: string;

    logradouro: string;

    bairro: string;

    cidade: string ;

    cliente: Cliente;

    uf: string;

    localidade: string;

    complemento: string;

}
