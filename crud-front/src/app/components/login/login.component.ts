import { ApiResponse } from './../../model/api.response';
import { User, IUser } from './../../model/user.model';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../service/user-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin = false;
  user: User;
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) { }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    // pegando dados de login
    const loginPayload = {
      username: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
    };

    const result = this.userService.login(loginPayload).subscribe(data => {

      if (data.body.tipoLogin.toString() === '1') {
        window.localStorage.setItem('token', data.body.token) //armazena token
        window.localStorage.setItem('role', 'ADMIN'); // pega role
        window.localStorage.setItem('userName', data.body.userName); // pega nome
        this.router.navigate(['dashboard']); // navega
        alert('Bem vindo: Admin ');
      }
      if (data.body.tipoLogin.toString() === '2') {
        window.localStorage.setItem('token', data.body.token) //armazena token
        window.localStorage.setItem('role', 'USER'); // pega role
        window.localStorage.setItem('userName', data.body.userName); // pega nome
        this.router.navigate(['dashboard']); // navega
        alert('Bem vindo: Usuário ');
      }
    });
  }

  ngOnInit() {
    if (window.localStorage.getItem('token') != null){
      this.router.navigate(['dashboard']);
    }
    window.localStorage.removeItem('token');
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]

    });
  }



}
