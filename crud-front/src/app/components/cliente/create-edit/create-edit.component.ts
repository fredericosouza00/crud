import { Email } from './../../../model/email.model';
import { ApiResponse } from './../../../model/api.response';
import { Cliente } from './../../../model/cliente.model';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/service/cliente-service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Endereco } from 'src/app/model/endereco.model';
import { Telefone } from 'src/app/model/telefone.model';


@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.css']
})
export class CreateEditComponent implements OnInit {
  /*OBJETOS*/
  role: string;
  cliente= new Cliente();

  telefone: string;
  listTelefone = [];

  email: string;
  listEmail = [];

  endereco = new Endereco();



  /*Configuracoes*/
  clienteId: string;
  isSaving: boolean;
  constructor(private router: Router, private clienteService: ClienteService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.clienteId = window.localStorage.getItem('clienteId');
    this.role = window.localStorage.getItem('role');

    if (this.clienteId != null) {
      this.clienteService.getClienteById(+this.clienteId).subscribe((res: HttpResponse<Cliente>) => {
        this.cliente = res.body;
        this.listEmail = res.body.email;
        this.listTelefone = res.body.telefone;
        this.endereco = res.body.endereco;

      });
    } else {
      this.cliente = new Cliente();
      this.cliente.endereco = new Endereco();
      this.cliente.email = [];
      this.cliente.telefone = [];



    }

  }

  save() {

    if (this.cliente.id == null) {
      this.listTelefone.forEach(telefone => {
        this.cliente.telefone.push(telefone);
      });

      this.listEmail.forEach(email => {

        this.cliente.email.push(email);
      });
      console.log(this.cliente);
      this.clienteService.createCliente(this.cliente).subscribe((res) => {
      });
    }
    else {
      this.cliente.email = [];
      this.cliente.telefone = []
      this.listTelefone.forEach(telefone => {
        this.cliente.telefone.push(telefone);
      });

      this.listEmail.forEach(email => {

        this.cliente.email.push(email);
      });
      this.clienteService.createCliente(this.cliente).subscribe((res) => {
      });

    }
    this.router.navigate(['dashboard']);

  }


  previousState() {
    window.localStorage.removeItem('clienteId');
    this.router.navigate(['dashboard']);
  }

  consultarCep(cep) {
    this.clienteService.getAddressByZipCode(cep)
      .subscribe((res) => {
        this.cliente.endereco.cep = res.cep;
        this.cliente.endereco.bairro = res.bairro;
        this.cliente.endereco.cidade = res.localidade;
        this.cliente.endereco.uf = res.uf;
        this.cliente.endereco.logradouro = res.logradouro;
      });

  }


  pushEmail(i): void {
    const email = new Email();
    email.email = i;
    this.listEmail.push(email);



  }

  pushTelefone(i): void {
    const telefone = new Telefone();
    telefone.numero = i;
    this.listTelefone.push(telefone);



  }

}
