import { Cliente } from './../../model/cliente.model';
import { ApiResponse } from './../../model/api.response';
import { Router } from '@angular/router';
import { ClienteService } from './../../service/cliente-service';
import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
clientes: Cliente[];
role: string ;
  constructor(private router: Router, private clienteService: ClienteService) { }

  ngOnInit(): void {
    this.carregarClientes()
    this.role = window.localStorage.getItem('role')

  }




  // CRUD


  createCliente(){
    this.router.navigate(['cliente']);
  }


  clienteDetail(clienteId: number){
    window.localStorage.setItem('clienteId', clienteId.toString());
    this.router.navigate(['cliente', clienteId]);
  }


  deleteCliente(id:number){
    this.clienteService.deleteCliente(id).subscribe((res: HttpResponse<Cliente>) => {
      console.log(res.body);
      this.carregarClientes()
    });


  }

  carregarClientes(){
    this.clienteService.getClientes().subscribe((res: HttpResponse<Cliente[]>) => {
      this.clientes = res.body;
    });
  }
}
