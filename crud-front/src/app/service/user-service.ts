import { User } from './../model/user.model';
import { ApiResponse } from './../model/api.response';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import {observable, Observable} from 'rxjs/index';
import { createRequestOption } from '../utils/req-utils';

type EntityResponseType = HttpResponse<ApiResponse>;
type EntityArrayResponseType = HttpResponse<ApiResponse[]>;
@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }
    baseUrl = 'http://localhost:8080/token';

    login(loginPayload): Observable<EntityResponseType> {
        const params: HttpParams = createRequestOption();
        const url = this.baseUrl + '/generate-token';
        return this.http.post<any>(url,loginPayload,{observe: 'response'});
    }

    getUsers(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl);
    }

    getUserByuser(user: string): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(this.baseUrl + user);
    }
}
