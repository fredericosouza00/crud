import { Cliente, ICliente } from './../model/cliente.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user.model';
import { Observable, observable } from 'rxjs/index';
import { ApiResponse } from '../model/api.response';
import { createRequestOption } from '../utils/req-utils';
import { Endereco } from '../model/endereco.model';

type EntityResponseType = HttpResponse<ICliente>;
type EntityArrayResponseType = HttpResponse<ICliente[]>;
@Injectable()
export class ClienteService {



  constructor(private http: HttpClient) { }
  baseUrl = 'http://localhost:8080/cliente';


  getClientes(): Observable<EntityArrayResponseType> {
    const params: HttpParams = createRequestOption();
    return this.http.get<ICliente[]>(this.baseUrl, { observe: 'response', params });
  }

  getClienteById(id: number): Observable<EntityResponseType> {
    const url = this.baseUrl + '/' + id;
    return this.http.get<Cliente>(url, { observe: 'response' });
  }

  deleteCliente(id: number): Observable<EntityResponseType> {

    const url = this.baseUrl + '/' + id;
    return this.http.delete<Cliente>(url, { observe: 'response' });

  }


  getAddressByZipCode(zipcode: string): Observable<Endereco> {
    const params: HttpParams = createRequestOption();
    const url2 = 'https://viacep.com.br/ws/'+ zipcode +'/json/'
    return this.http.get<Endereco>(url2, {params});
  }




  createCliente(cliente: Cliente): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json;');
    const url = this.baseUrl;
    return this.http.post<Cliente>(url, cliente, {observe: 'response', headers});
}


updateCliente(cliente: Cliente): Observable<EntityResponseType> {
  let headers = new HttpHeaders();
  headers = headers.set('Content-Type', 'application/json;');
  const url = this.baseUrl;
  return this.http.post<Cliente>(url, cliente, {observe: 'response', headers});


}
}
