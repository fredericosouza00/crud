import { ClienteService } from './service/cliente-service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {routing} from './app.routing';
import {Router, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UserService} from './service/user-service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { CreateEditComponent } from './components/cliente/create-edit/create-edit.component';
import { FormsModule } from '@angular/forms';
import { TokenInterceptor } from './core/interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    CreateEditComponent
  ],
    imports: [
        BrowserModule,
        routing,
        ReactiveFormsModule,
        RouterModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [UserService, ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
