import { CreateEditComponent } from './components/cliente/create-edit/create-edit.component';
import { HomeComponent } from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';

const routes: Routes = [
{ path: '', component: LoginComponent },
{path: 'home', component: HomeComponent},
{path: 'dashboard', component: DashboardComponent},
{path: 'cliente/:id', component: CreateEditComponent},
{path: 'cliente', component: CreateEditComponent}

];
export const routing = RouterModule.forRoot(routes);
