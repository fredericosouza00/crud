package fred.example.crudProject.domain;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Transactional
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE )
    private Long id;

    @Column
    private String nome;

    @Column
    private String cpf;


    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true,fetch=FetchType.EAGER)
    private Endereco endereco ;



    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch=FetchType.EAGER )
    private Set<Telefone> telefone = new HashSet<>();



    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch=FetchType.EAGER)
    private Set<Email> email = new HashSet<>();


    public Cliente(Long id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }

    public Cliente() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Set<Telefone> getTelefone() {
        return telefone;
    }

    public void setTelefone(Set<Telefone> endetelefonereco) {
        this.telefone = endetelefonereco;
    }

    public Set<Email> getEmail() {
        return email;
    }

    public void setEmail(Set<Email> email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", endereco=" + endereco +
                ", endetelefonereco=" + telefone +
                ", email=" + email +
                '}';
    }
}


