package fred.example.crudProject.domain;

import org.apache.catalina.User;

public class AuthToken {

    private String token;
    private String userName;
    private Integer tipoLogin;


    public AuthToken(){

    }

    public AuthToken(String token, String user,Integer tipoLogin){
        this.token = token;
        this.userName = user;
        this.tipoLogin = tipoLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getTipoLogin() {
        return tipoLogin;
    }

    public void setTipoLogin(Integer tipoLogin) {
        this.tipoLogin = tipoLogin;
    }
}
