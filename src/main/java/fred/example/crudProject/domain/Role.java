package fred.example.crudProject.domain;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;


public class Role implements GrantedAuthority {


    private  String nomeRole;

    public String getNomeRole() {
        return nomeRole;
    }

    public void setNomeRole(String nomeRole) {
        this.nomeRole = nomeRole;
    }

    @Override
    public String getAuthority() {

        return this.nomeRole;
    }


}
