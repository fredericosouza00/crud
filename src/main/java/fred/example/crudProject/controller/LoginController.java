package fred.example.crudProject.controller;

import fred.example.crudProject.security.JwtTokenUtil;
import fred.example.crudProject.domain.ApiResponse;
import fred.example.crudProject.domain.AuthToken;
import fred.example.crudProject.domain.UserCrud;
import fred.example.crudProject.dto.UserLoginDTO;
import fred.example.crudProject.repository.UserCrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class LoginController {

    final private AuthenticationManager authenticationManager;
    final private  JwtTokenUtil jwtTokenUtil;
    final private UserCrudRepository userCrudRepository;

    public LoginController(AuthenticationManager authenticationManager,JwtTokenUtil jwtTokenUtil,UserCrudRepository userCrudRepository){
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userCrudRepository = userCrudRepository;
    }

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public AuthToken register(@RequestBody UserLoginDTO userLoginDTO) throws AuthenticationException {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userLoginDTO.getUsername(), userLoginDTO.getPassword()));
            final UserCrud userCrud = userCrudRepository.findAllByLogin(userLoginDTO.getUsername());
            final String token = jwtTokenUtil.generateToken(userCrud);

            //admin
            if(userCrud.getTipoLogin() == 1) {
                return new AuthToken(token,userCrud.getLogin(),userCrud.getTipoLogin());
            }

            //comum
            if(userCrud.getTipoLogin() == 2) {
                return new AuthToken(token,userCrud.getLogin(),userCrud.getTipoLogin());
            }

            return new AuthToken(token,userCrud.getLogin(),userCrud.getTipoLogin());

        }catch(AuthenticationException authException) {
            return null;
        }

    }

}

