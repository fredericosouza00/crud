package fred.example.crudProject.controller;

import fred.example.crudProject.domain.*;
import fred.example.crudProject.dto.ClienteDTO;
import fred.example.crudProject.repository.ClienteRepository;
import fred.example.crudProject.repository.EmailRepository;
import fred.example.crudProject.repository.EnderecoRepository;
import fred.example.crudProject.repository.TelefoneRepository;
import fred.example.crudProject.service.ClienteService;
import fred.example.crudProject.service.EmailService;
import fred.example.crudProject.service.EnderecoService;
import fred.example.crudProject.service.TelefoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelefoneService telefoneService;

    @Autowired
    private EnderecoService enderecoService;

    @GetMapping
    public ResponseEntity<List<Cliente>> getClientes() {

        List<Cliente> clienteList = clienteService.buscarResumo();


        return new ResponseEntity<List<Cliente>>(clienteList,HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> getCliente(@PathVariable Long id ){

        Cliente cliente = clienteService.findOneById(id);

        return new ResponseEntity<>(cliente,HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    public ResponseEntity<Cliente> delete(@PathVariable Long id) {
        String resultado = clienteService.deleteOneById(id);
        System.out.println(resultado);
        return new ResponseEntity<>(null, HttpStatus.OK);

    }
    @PostMapping
    public void save(@RequestBody Cliente cliente){
        clienteService.salvarCliente(cliente);
//        telefoneService.salvarTelefones(cliente.getTelefone());
//        enderecoService.salvarEndereco(cliente.getEndereco());
//        emailService.salvarEmail(cliente.getEmail());




        //cliente.setEmail(emailRepository);
        //clienteRepository.save(cliente);
    }

//    @PutMapping("/{id}")
//    public void update(@RequestBody Cliente cliente) {
//        clienteRepository.saveAndFlush(cliente);
//    }
}
