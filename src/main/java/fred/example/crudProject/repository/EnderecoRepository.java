package fred.example.crudProject.repository;

import fred.example.crudProject.domain.Email;
import fred.example.crudProject.domain.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco,Long> {

    @Transactional
    @Query("SELECT en FROM Endereco en WHERE en.cliente.id in (?1) ")
    List<Endereco> findEnderecoByClienteId(Long id);

}
