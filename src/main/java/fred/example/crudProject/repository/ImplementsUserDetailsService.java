package fred.example.crudProject.repository;

import fred.example.crudProject.domain.UserCrud;
import fred.example.crudProject.repository.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class ImplementsUserDetailsService implements UserDetailsService {
    @Autowired
    private UserCrudRepository userCrudRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
         UserCrud userCrud = userCrudRepository.findAllByLogin(login);

        if(userCrud == null){
            throw new UsernameNotFoundException("Usuário não encontrado");
        }
        return userCrud;
    }
}
