package fred.example.crudProject.repository;

import fred.example.crudProject.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Long> {



    @Query("SELECT new fred.example.crudProject.domain.Cliente(c.id,c.nome,c.cpf)FROM Cliente c ")
    List<Cliente> findAllResumo();







}
