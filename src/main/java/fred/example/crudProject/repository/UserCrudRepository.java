package fred.example.crudProject.repository;

import fred.example.crudProject.domain.UserCrud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCrudRepository extends JpaRepository<UserCrud,String> {
    UserCrud findAllByLogin(String login);

}
