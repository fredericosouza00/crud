package fred.example.crudProject.repository;

import fred.example.crudProject.domain.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TelefoneRepository extends JpaRepository<Telefone,Long> {

    @Transactional
    @Query("SELECT t FROM Telefone t WHERE t.cliente.id in (?1)" )
    List<Telefone> findTelefoneByClienteId(Long clienteId);



}
