package fred.example.crudProject.repository;

import fred.example.crudProject.domain.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EmailRepository extends JpaRepository<Email,Long> {

    @Transactional
    @Query("SELECT em  FROM Email em  WHERE em.cliente.id in (?1) ")
    List<Email> findEmailByClienteId(Long id);

}
