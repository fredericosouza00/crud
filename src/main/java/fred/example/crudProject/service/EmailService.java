package fred.example.crudProject.service;

import fred.example.crudProject.domain.Email;
import fred.example.crudProject.domain.Endereco;
import fred.example.crudProject.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailService {
    @Autowired
    private EmailRepository emailRepository;

    public void salvarEmail(List<Email> listEmail){
        emailRepository.saveAll(listEmail);
    }
}
