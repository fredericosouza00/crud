package fred.example.crudProject.service;

import fred.example.crudProject.domain.Endereco;
import fred.example.crudProject.domain.Telefone;
import fred.example.crudProject.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService {
    @Autowired
    private EnderecoRepository enderecoRepository;

    public void salvarEndereco(List<Endereco> listEndereco){
        enderecoRepository.saveAll(listEndereco);
    }
}
