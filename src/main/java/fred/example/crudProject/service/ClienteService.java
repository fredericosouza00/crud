package fred.example.crudProject.service;

import fred.example.crudProject.domain.Cliente;
import fred.example.crudProject.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public void salvarCliente(Cliente cliente){
        clienteRepository.saveAndFlush(cliente);
      //  clienteRepository.save(cliente);

    }
    public void updateCliente(Cliente cliente){

    }


    public List<Cliente> buscarResumo(){
        List<Cliente> clienteList = clienteRepository.findAllResumo();
        return clienteList;
    }

    public  Cliente findOneById(Long id){
        Cliente cliente = clienteRepository.findById(id).get();
        return cliente;

    }

    public String deleteOneById(long id){
        clienteRepository.deleteById(id);
        return "Deletado";
    }



}
