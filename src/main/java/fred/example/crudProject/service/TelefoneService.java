package fred.example.crudProject.service;

import fred.example.crudProject.domain.Telefone;
import fred.example.crudProject.repository.TelefoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TelefoneService {

    @Autowired
    private TelefoneRepository telefoneRepository;

    public void salvarTelefones(List<Telefone> listTelefone){
        telefoneRepository.saveAll(listTelefone);
    }
}
